/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lamuclient.Common;

import java.io.BufferedWriter;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Writer;
import java.util.Date;
import java.util.Properties;
import java.util.logging.Level;
import javax.swing.JOptionPane;
import lamuclient.I.Bitacora;

/**
 *
 * @author santi
 */
public class Logger implements Bitacora { 

    @Override
    public void LogAction(String message, String action) {
        try{
            Writer output;
            output = new BufferedWriter(new FileWriter("lamu.log", true));  //clears file every time
            output.append("\n"+(new Date()).toString()+"-"+action+": "+message);
            output.close();
        } catch (FileNotFoundException e){
            JOptionPane.showMessageDialog(null, "No se encontro el archivo de logs");
        } catch (IOException ex) {
            JOptionPane.showMessageDialog(null, "No se puede abrir el archivo de logs");
        }  
    }

    @Override
    public void LogException(String message, Exception et) {
        try{
            Writer output;
            output = new BufferedWriter(new FileWriter("lamu.err", true));  //clears file every time
            output.append("\n"+(new Date()).toString()+"-"+et.getMessage()+": "+message);
            output.close();
        } catch (FileNotFoundException e){
            JOptionPane.showMessageDialog(null, "No se encontro el archivo de logs");
        } catch (IOException ex) {
            JOptionPane.showMessageDialog(null, "No se puede abrir el archivo de logs");
        }  
    }
    
}
