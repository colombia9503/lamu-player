/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lamuclient.Common;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

/**
 *
 * @author santi
 */
public class Conexion {
    private Logger log;
    private Connection con = null;
    
    private final String BD = "lamusa";
    private final String udb = "root";
    private final String password = "";
    private final String URL = "jdbc:mysql://localhost:3306/" + BD;
    
    public Connection getInstance() {
        return con;
    }
    
    public Conexion(Logger log) {
        this.log = log;
        try {
            Class.forName("com.mysql.jdbc.Driver");
            con = DriverManager.getConnection(URL, udb, password);
            if (con != null) {
                log.LogAction("Conexión a la BD exitosa!", "Conexion a BD");
            }
        } catch (SQLException e) {
            log.LogException("Error de conexión a la base de datos", e);
        } catch (ClassNotFoundException e) {
            log.LogException("Driver de conexion con mysql no encontrado", e);
        }
    }
    
    public void destroy() {
        con = null;
    }

    /**
     * @return the BD
     */
    public String getBD() {
        return BD;
    }

    /**
     * @return the URL
     */
    public String getURL() {
        return URL;
    }

    /**
     * @return the udb
     */
    public String getUdb() {
        return udb;
    }

    /**
     * @return the password
     */
    public String getPassword() {
        return password;
    }
}
