/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lamuclient.DTO;

import java.sql.SQLException;
import lamuclient.Common.Conexion;
import lamuclient.Common.Logger;
import lamuclient.DAO.UsuariosDao;
import lamuclient.POJOS.Usuario;
import lamuclient.UI.MenuPrincipal;
import org.apache.commons.codec.digest.DigestUtils;

/**
 *
 * @author santi
 */
public class LoginDto {
    private Logger log;

    public LoginDto(Logger log) {
        this.log = log;
    }
    
    public boolean verificarUsuarioPorEmail(Usuario user) {
        Conexion con = new Conexion(log);
        UsuariosDao gestor = new UsuariosDao(con);
        try {
            Usuario busquedaUsuario = gestor.getUserByEmail(user.getEmail());
            if(busquedaUsuario != null) {
                String claveIngresada = DigestUtils.sha512Hex(DigestUtils.sha512Hex(user.getPassword()) + busquedaUsuario.getSalt());
                if(claveIngresada.equals(busquedaUsuario.getPassword())){
                    MenuPrincipal.nombreUsuario = busquedaUsuario.getNombres();
                    MenuPrincipal.idUsuario = busquedaUsuario.getId();
                    MenuPrincipal.rol = busquedaUsuario.getRol();
                    return true;
                }
            }
        } catch (SQLException e) {
            log.LogException("verificarUsuarioPorEmail()", e);
            e.printStackTrace();
        }
        return false;
    }
}
