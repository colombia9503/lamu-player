/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lamuclient.DTO;

import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.sql.SQLException;
import javax.swing.table.DefaultTableModel;
import lamuclient.Common.Conexion;
import lamuclient.Common.Logger;
import lamuclient.Common.RsTableModel;
import lamuclient.DAO.PistasDao;
import lamuclient.POJOS.Pista;
import org.apache.commons.io.FileUtils;

import java.net.MalformedURLException;
import java.net.URL;
import javazoom.jlgui.basicplayer.BasicPlayer;
import javazoom.jlgui.basicplayer.BasicPlayerException;

/**
 *
 * @author santi
 */
public class PistasDto {

    private Logger log;
    BasicPlayer player;

    public PistasDto(Logger log) {
        this.log = log;
        this.player = new BasicPlayer();
    }

    public DefaultTableModel obtenerDatosDePistas() {
        try {
            Conexion con = new Conexion(log);
            PistasDao gestor = new PistasDao(con);
            return RsTableModel.buildTableModel(gestor.get());
        } catch (SQLException e) {
            log.LogException("getPistasData()", e);
        }
        return new DefaultTableModel();
    }

    public boolean salvarNuevaPista(Pista pista, File origen) {
        try {
            Conexion con = new Conexion(log);
            PistasDao gestor = new PistasDao(con);
            File destino = new File(System.getProperty("user.dir") + "/src/lamuclient/Resources/audio/" + pista.toString());
            pista.setPath("/src/lamuclient/Resources/audio/" + pista.toString());
            FileUtils.copyFile(origen, destino);
            return gestor.save(pista);
        } catch (SQLException e) {
            log.LogException("savePistasMeta() SQL", e);
        } catch (IOException e) {
            log.LogException("savePistasMeta() IO", e);
        }
        return false;
    }

    public void playMusic(Pista pista) {
        String pathToMp3 = pista.getPath();
        try {
            player.open(new URL("file:///" + System.getProperty("user.dir") + pathToMp3));
            player.play();
        } catch (BasicPlayerException | MalformedURLException e) {
            log.LogException("Play Music()", e);
        }
    }

    public void stopMusic() {
        try {
            player.stop();
        } catch (BasicPlayerException e) {
            log.LogException("StopMusic()", e);
        }
    }
    
    public boolean verificacionPistaYMetadatos(File archivo, Pista pista){
        return archivo != null && pista != null;
    }
}
