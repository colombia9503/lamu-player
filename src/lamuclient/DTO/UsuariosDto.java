/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lamuclient.DTO;

import java.math.BigInteger;
import java.security.SecureRandom;
import java.sql.SQLException;
import javax.swing.JOptionPane;
import javax.swing.table.DefaultTableModel;
import lamuclient.Common.Conexion;
import lamuclient.Common.Logger;
import lamuclient.Common.RsTableModel;
import lamuclient.DAO.UsuariosDao;
import lamuclient.POJOS.Usuario;
import org.apache.commons.codec.digest.DigestUtils;

/**
 *
 * @author santi
 */
public class UsuariosDto {
    private Logger log;

    public UsuariosDto(Logger log) {
        this.log = log;
    }
    
    public boolean verificarClave(String ingresada, String verificada) {
        return ingresada.equals(verificada);
    }
    
    public DefaultTableModel obtenerDatosDeUsuarios() {
        try {
            Conexion con = new Conexion(log);
            UsuariosDao gestor = new UsuariosDao(con);
            return RsTableModel.buildTableModel(gestor.get());
        } catch (SQLException e) {
            log.LogException("getPistasData()", e);
            e.printStackTrace();
        }
        return new DefaultTableModel();
    }
    
    public boolean registrarNuevoUsuario(Usuario user) {
        try {
            Conexion con = new Conexion(log);
            UsuariosDao gestor = new UsuariosDao(con);
            SecureRandom random = new SecureRandom();
            user.setSalt(new BigInteger(640, random).toString());
            user.setPassword(DigestUtils.sha512Hex(DigestUtils.sha512Hex(user.getPassword()) + user.getSalt()));
            
            return gestor.save(user);
        } catch (SQLException e) {
            log.LogException("registratUsuario() SQL", e);
        }
        return false;
    }
    
    public boolean borrarUsuario(Usuario user) {
        try {
            Conexion con = new Conexion(log);
            UsuariosDao gestor = new UsuariosDao(con);
            return gestor.delete(user);
        } catch (SQLException e) {
            log.LogException("borrarUsuario() SQL", e);
        }
        return false;
    }
    
    public boolean actualizarUsuario(Usuario user) {
        try {
            Conexion con = new Conexion(log);
            UsuariosDao gestor = new UsuariosDao(con);
            
            SecureRandom random = new SecureRandom();
            user.setSalt(new BigInteger(640, random).toString());
            user.setPassword(DigestUtils.sha512Hex(DigestUtils.sha512Hex(user.getPassword()) + user.getSalt()));
            
            return gestor.update(user);
        } catch (SQLException e) {
            log.LogException("actualizarUsuario() SQL", e);
        }
        return false;
    }
    
    public boolean validateEmail(String email) {
        if(!email.contains("@") && !email.contains("."))
            return false;
        
        return true;
    }
}
