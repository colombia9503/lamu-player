/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lamuclient.POJOS;

import java.util.UUID;

/**
 *
 * @author santi
 */
public class Pista {
    private int id;
    private String nombre;
    private int autor;
    private String duracion;
    private String genero;
    private String descripcion;
    private String albumn;
    private String path;

    public Pista() {
    }

    public Pista(int id, String nombre, int autor, String duracion, String genero, String descripcion, String albumn) {
        this.id = id;
        this.nombre = nombre;
        this.autor = autor;
        this.duracion = duracion;
        this.genero = genero;
        this.descripcion = descripcion;
        this.albumn = albumn;
    }
    
    public Pista (String nombre, int autor, String duracion, String genero, String descripcion, String albumn) {
        this.id = id;
        this.nombre = nombre;
        this.autor = autor;
        this.duracion = duracion;
        this.genero = genero;
        this.descripcion = descripcion;
        this.albumn = albumn;
    }

    /**
     * @return the id
     */
    public int getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(int id) {
        this.id = id;
    }

    /**
     * @return the nombre
     */
    public String getNombre() {
        return nombre;
    }

    /**
     * @param nombre the nombre to set
     */
    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    /**
     * @return the autor
     */
    public int getAutor() {
        return autor;
    }

    /**
     * @param autor the autor to set
     */
    public void setAutor(int autor) {
        this.autor = autor;
    }

    /**
     * @return the duración
     */
    public String getDuracion() {
        return duracion;
    }

    /**
     * @param duración the duración to set
     */
    public void setDuracion(String duracion) {
        this.duracion = duracion;
    }

    /**
     * @return the genero
     */
    public String getGenero() {
        return genero;
    }

    /**
     * @param genero the genero to set
     */
    public void setGenero(String genero) {
        this.genero = genero;
    }

    /**
     * @return the descripcion
     */
    public String getDescripcion() {
        return descripcion;
    }

    /**
     * @param descripcion the descripcion to set
     */
    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    /**
     * @return the albumn
     */
    public String getAlbumn() {
        return albumn;
    }

    /**
     * @param albumn the albumn to set
     */
    public void setAlbumn(String albumn) {
        this.albumn = albumn;
    }

    /**
     * @return the path
     */
    public String getPath() {
        return path;
    }

    /**
     * @param path the path to set
     */
    public void setPath(String path) {
        this.path = path;
    }

    @Override
    public String toString() {
        return (nombre+"-"+autor+"-"+albumn+"-"+genero+".mp3").replaceAll("\\s+","");
    }
    
    
}
