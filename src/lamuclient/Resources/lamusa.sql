CREATE DATABASE  IF NOT EXISTS `lamusa` /*!40100 DEFAULT CHARACTER SET latin1 */;
USE `lamusa`;
-- MySQL dump 10.13  Distrib 5.7.17, for Win64 (x86_64)
--
-- Host: 127.0.0.1    Database: lamusa
-- ------------------------------------------------------
-- Server version	5.7.14

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `pistas`
--

DROP TABLE IF EXISTS `pistas`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `pistas` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(45) NOT NULL,
  `autor` int(11) NOT NULL,
  `duracion` varchar(45) NOT NULL,
  `genero` varchar(45) NOT NULL,
  `descripcion` varchar(200) DEFAULT NULL,
  `albumn` varchar(45) DEFAULT NULL,
  `path` varchar(255) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `autor_idx` (`autor`),
  CONSTRAINT `autor` FOREIGN KEY (`autor`) REFERENCES `usuarios` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `pistas`
--

LOCK TABLES `pistas` WRITE;
/*!40000 ALTER TABLE `pistas` DISABLE KEYS */;
INSERT INTO `pistas` VALUES (2,'vete a la verga',2,'3:00','pop','que te valga verga','verguisas','/src/lamuclient/Resources/audio/vetealaverga-2-verguisas-pop'),(6,'nombre',2,'duracion','genero','descripcion','albumn','/src/lamuclient/Resources/audio/nombre-2-albumn-genero.mp3'),(7,'porque no se van',2,'3:00','rock en español','cancion subida','albumnacion','/src/lamuclient/Resources/audio/porquenosevan-2-albumnacion-rockenespañol.mp3'),(8,'me hueli el celuco',2,'3:00','pop ','exitos del ayer y el hoy','exitos del momento','/src/lamuclient/Resources/audio/mehuelielceluco-2-exitosdelmomento-pop.mp3'),(9,'demencia',2,'3:00','rock','excelente ambiente recreativo','albumn musical','/src/lamuclient/Resources/audio/demencia-2-albumnmusical-rock.mp3');
/*!40000 ALTER TABLE `pistas` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `roles`
--

DROP TABLE IF EXISTS `roles`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `roles` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(100) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `roles`
--

LOCK TABLES `roles` WRITE;
/*!40000 ALTER TABLE `roles` DISABLE KEYS */;
INSERT INTO `roles` VALUES (1,'Administrador'),(2,'Cliente'),(3,'Productor Musical');
/*!40000 ALTER TABLE `roles` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `usuarios`
--

DROP TABLE IF EXISTS `usuarios`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `usuarios` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nombres` varchar(100) NOT NULL,
  `apellidos` varchar(100) DEFAULT NULL,
  `email` varchar(100) NOT NULL,
  `rol` int(11) NOT NULL,
  `password` char(255) NOT NULL,
  `salt` char(255) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `email_UNIQUE` (`email`),
  KEY `rol_idx` (`rol`),
  CONSTRAINT `rol` FOREIGN KEY (`rol`) REFERENCES `roles` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `usuarios`
--

LOCK TABLES `usuarios` WRITE;
/*!40000 ALTER TABLE `usuarios` DISABLE KEYS */;
INSERT INTO `usuarios` VALUES (2,'santiago','umana','admin',1,'15df10541b097b83f2256fa511114ea01c10dc6b95f844f523564d33645628c5d0a23cabead43eca07ebcc1f2ffa22a1221f5105967b89ef191a0dc2145d2c22','933946955104739858629122183205967643676894440641868671125853956455275480251228076549359557119013013414380135880813590280180683344177052594063877108238298300793940569394391510524691589805650311'),(3,'usuario1','usuario1','productor',3,'3c53d45c90f245a31316005e15fea712a827daad093525ce49b42a917a1e6f9daf962640456f0fdea4cff314f9c0d613288a754f52a343dbcb0791d604dd07ae','1420730027865582740187099567743667867361314612070235670295948853860869284505919222824111019414604721449011520610435654524079915494788614753259123668372581522564144112687558074523085689805390783'),(4,'cliente','cliente','cliente',2,'d0ab74951aeb5f829d73646a04e398c27fc83b780790a65b6adec9a9ae959b264d37e44786e2abb48c0e896ef12f64e62fd813833b4c4486dd24ff2d412bffbb','444596527661899813556273361776008319537905947168745065935436076146599801590843350780163079216508633462967298772098305136328354314888087335150816938833831232022516424501441051276875120382834112'),(8,'test','test','test',1,'8c0923401dd91ebb14c947a54f050593908c3e377bdc0e46e94d3ef6d805fabe596eca0c94f342677fd41e890709e3f9878541001d3437b54721c4ed7274a356','4318004084495526585884304677567365575223520629209279981559834646097244797263742244045793628786851944848772628101861980534714225338134803096351379279857918084160646633811474667411810537268384555'),(12,'juanito','de tales','juanitodtales123',1,'5c9c58060b1eeec4fcfd58ae7df75ea6120ec068e22efad4a4b6fd724cf1646a55536bb4345c920b3151c1e28a1d17d7bf4032db03332a0702a3f81a017e5eb5','3958212803433123815917174203075895494120789002404978015225886899503244433961212241484056667578789753639744735730887948308118933322948232720065172533945489410156819031932984862907071156622234696');
/*!40000 ALTER TABLE `usuarios` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2017-11-04 12:17:45
