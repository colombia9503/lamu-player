/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lamuclient.I;

import java.sql.SQLException;

/**
 *
 * @author santi
 * @param <E>
 */
public interface AuthDao<E> extends Dao<E> {
    public E getUserByEmail(String email) throws SQLException;
}
