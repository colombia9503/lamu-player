/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lamuclient.I;

import java.sql.ResultSet;
import java.sql.SQLException;
import javax.swing.DefaultComboBoxModel;

/**
 *
 * @author santi
 */
public interface Dao<E> {
    public ResultSet get() throws SQLException;
    public E getById(int id) throws SQLException;
    public boolean save(E object) throws SQLException;
    public boolean update(E object) throws SQLException;
    public boolean delete(E object) throws SQLException;
    public boolean exists(E object) throws SQLException;
    public abstract DefaultComboBoxModel<E> jcbObjects() throws SQLException;
}
