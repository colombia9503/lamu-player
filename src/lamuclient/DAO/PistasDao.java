/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lamuclient.DAO;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import javax.swing.DefaultComboBoxModel;
import lamuclient.Common.Conexion;
import lamuclient.I.Dao;
import lamuclient.POJOS.Pista;
import lamuclient.POJOS.Usuario;

/**
 *
 * @author santi
 */
public class PistasDao implements Dao<Pista> {
    Conexion con;

    public PistasDao(Conexion con) {
        this.con = con;
    }
    
    @Override
    public ResultSet get() throws SQLException {
        ResultSet result = null;
        String query = "SELECT * FROM pistas;";
        PreparedStatement pstm = con.getInstance().prepareCall(query);
        result = pstm.executeQuery();
        return result;
    }

    @Override
    public Pista getById(int id) throws SQLException {
        ResultSet result = null;
        Pista pista = new Pista();
        String query = "SELECT * FROM pistas WHERE id = ?;";
        PreparedStatement pstm = con.getInstance().prepareCall(query);
        pstm.setInt(1, id);
        result = pstm.executeQuery();
        if(result.next()){
            pista.setAlbumn(result.getString("albumn"));
            pista.setAutor(result.getInt("autor"));
            pista.setDescripcion(result.getString("descripcion"));
            pista.setDuracion(result.getString("duracion"));
            pista.setGenero(result.getString("genero"));
            pista.setId(result.getInt("id"));
            pista.setNombre(result.getString("nombre"));
            pista.setPath(result.getString("path"));
        }
        return pista;
    }

    @Override
    public boolean save(Pista object) throws SQLException {
        String query = "INSERT INTO pistas (nombre, autor, duracion, genero, descripcion, albumn, path) "
                + "VALUES (?, ?, ?, ?, ?, ?, ?)";
        PreparedStatement pstm = con.getInstance().prepareCall(query);
        pstm.setString(1, object.getNombre());
        pstm.setInt(2, object.getAutor());
        pstm.setString(3, object.getDuracion());
        pstm.setString(4, object.getGenero());
        pstm.setString(5, object.getDescripcion());
        pstm.setString(6, object.getAlbumn());
        pstm.setString(7, object.getPath());
        
        int result = pstm.executeUpdate();
        if (result != 0)
            return true;
        else 
            return false;
    }

    @Override
    public boolean update(Pista object) throws SQLException {
        String query = "UPDATE pistas SET nombre = ?, autor = ?, duracion = ?, genero = ?, descripcion = ?, albumn = ?, path = ? "
                + "WHERE id = ?;";
        PreparedStatement pstm = con.getInstance().prepareCall(query);
        pstm.setString(1, object.getNombre());
        pstm.setInt(2, object.getAutor());
        pstm.setString(3, object.getDuracion());
        pstm.setString(4, object.getGenero());
        pstm.setString(5, object.getDescripcion());
        pstm.setString(6, object.getAlbumn());
        pstm.setString(7, object.getPath());
        pstm.setInt(8, object.getId());
        
        int result = pstm.executeUpdate();
        if (result != 0)
            return true;
        else 
            return false;
    }

    @Override
    public boolean delete(Pista object) throws SQLException {
        String query = "DELETE FROM pistas WHERE id = ?;";
        PreparedStatement pstm = con.getInstance().prepareCall(query);
        pstm.setInt(1, object.getId());
        int result = pstm.executeUpdate();
        if (result != 0)
            return true;
        else 
            return false;
    }

    @Override
    public boolean exists(Pista object) throws SQLException {
        ResultSet result = null;
        String query = "SELECT * FROM pistas WHERE id = ?;";
        PreparedStatement pstm = con.getInstance().prepareCall(query);
        pstm.setInt(1, object.getId());
        result = pstm.executeQuery();
        
        if(result.next())
            return true;
        else
            return false;
    }
  
    @Override
    public DefaultComboBoxModel<Pista> jcbObjects() throws SQLException {
        ResultSet result = null;
        DefaultComboBoxModel<Pista> cbModel = new DefaultComboBoxModel<>();
        String query = "SELECT * FROM pistas;";
        PreparedStatement pstm = con.getInstance().prepareCall(query);
        result = pstm.executeQuery();
        Pista pista;
        while (result.next()) {
            pista = new Pista();
            pista.setAlbumn(result.getString("albumn"));
            pista.setAutor(result.getInt("autor"));
            pista.setDescripcion(result.getString("descripcion"));
            pista.setDuracion(result.getString("duracion"));
            pista.setGenero(result.getString("genero"));
            pista.setId(result.getInt("id"));
            pista.setNombre(result.getString("nombre"));
            pista.setPath(result.getString("path"));
            cbModel.addElement(pista);
        }
        return cbModel;
    }
    
}
