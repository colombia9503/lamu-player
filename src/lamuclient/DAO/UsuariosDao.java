/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lamuclient.DAO;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import javax.swing.DefaultComboBoxModel;
import lamuclient.Common.Conexion;
import lamuclient.I.AuthDao;
import lamuclient.I.Dao;
import lamuclient.POJOS.Usuario;

/**
 *
 * @author santi
 */
public class UsuariosDao implements AuthDao<Usuario>{
    Conexion con;

    public UsuariosDao(Conexion con) {
        this.con = con;
    } 

    @Override
    public Usuario getUserByEmail(String email) throws SQLException {
        ResultSet result = null;
        Usuario usuario = new Usuario();
        String query = "SELECT * FROM usuarios WHERE email = ?;";
        PreparedStatement pstm = con.getInstance().prepareCall(query);
        pstm.setString(1, email);
        result = pstm.executeQuery();
        if(result.next()){
            usuario.setId(result.getInt("id"));
            usuario.setNombres(result.getString("nombres"));
            usuario.setApellidos(result.getString("apellidos"));
            usuario.setEmail(result.getString("email"));
            usuario.setRol(result.getInt("rol"));
            usuario.setPassword(result.getString("password"));
            usuario.setSalt(result.getString("salt"));
            return usuario;
        }
        return null;
    }

    @Override
    public ResultSet get() throws SQLException {
        ResultSet result = null;
        String query = "SELECT u.id, nombres, apellidos, email, r.nombre as rol FROM usuarios u, roles r "
                + "WHERE r.id = u.rol;";
        PreparedStatement pstm = con.getInstance().prepareCall(query);
        result = pstm.executeQuery();
        return result;    
    }

    @Override
    public Usuario getById(int id) throws SQLException {
        ResultSet result = null;
        Usuario pista = new Usuario();
        String query = "SELECT * FROM usuarios WHERE id = ?;";
        PreparedStatement pstm = con.getInstance().prepareCall(query);
        pstm.setInt(1, id);
        result = pstm.executeQuery();
        if(result.next()){
            return pista;
        }
        return null;
    }

    @Override
    public boolean save(Usuario object) throws SQLException {
        String query = "INSERT INTO usuarios (nombres, apellidos, email, rol, password, salt) "
                + "VALUES (?, ?, ?, ?, ?, ?)";
        PreparedStatement pstm = con.getInstance().prepareCall(query);
        pstm.setString(1, object.getNombres());
        pstm.setString(2, object.getApellidos());
        pstm.setString(3, object.getEmail());
        pstm.setInt(4, object.getRol());
        pstm.setString(5, object.getPassword());
        pstm.setString(6, object.getSalt());
        
        int result = pstm.executeUpdate();
        if (result != 0)
            return true;
        else 
            return false;
    }

    @Override
    public boolean update(Usuario object) throws SQLException {
        String query = "UPDATE usuarios SET nombres = ?, apellidos = ?, email = ?, rol = ?, password = ?, salt = ? "
                + "WHERE id = ?;";
        PreparedStatement pstm = con.getInstance().prepareCall(query);
        pstm.setString(1, object.getNombres());
        pstm.setString(2, object.getApellidos());
        pstm.setString(3, object.getEmail());
        pstm.setInt(4, object.getRol());
        pstm.setString(5, object.getPassword());
        pstm.setString(6, object.getSalt());
        pstm.setInt(7, object.getId());
        
        int result = pstm.executeUpdate();
        if (result != 0)
            return true;
        else 
            return false;
    }

    @Override
    public boolean delete(Usuario object) throws SQLException {
        String query = "DELETE FROM usuarios WHERE id = ?;";
        PreparedStatement pstm = con.getInstance().prepareCall(query);
        pstm.setInt(1, object.getId());
        int result = pstm.executeUpdate();
        if (result != 0)
            return true;
        else 
            return false;    }

    @Override
    public boolean exists(Usuario object) throws SQLException {
        ResultSet result = null;
        String query = "SELECT * FROM usuarios WHERE id = ?;";
        PreparedStatement pstm = con.getInstance().prepareCall(query);
        pstm.setInt(1, object.getId());
        result = pstm.executeQuery();
        
        if(result.next())
            return true;
        else
            return false;    }

    @Override
    public DefaultComboBoxModel jcbObjects() throws SQLException {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
}
