/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import lamuclient.Common.Logger;
import lamuclient.DTO.LoginDto;
import lamuclient.POJOS.Usuario;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author santi
 */
public class LoginDtoTest {
    
    public LoginDtoTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
        
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }
    
    @Test
    public void UsuarioEncontradoPorEmailConClaveCorrecta() {
        Usuario user = new Usuario();
        user.setEmail("admin");
        user.setPassword("1234");
        LoginDto dto = new LoginDto(new Logger());
        assertTrue(dto.verificarUsuarioPorEmail(user));
    }
    
    @Test
    public void UsuarioEncontradoPorEmailConClaveIncorrecta() {
        Usuario user = new Usuario();
        user.setEmail("admin");
        user.setPassword("44444");
        LoginDto dto = new LoginDto(new Logger());
        assertFalse(dto.verificarUsuarioPorEmail(user));
    }
    
    @Test
    public void UsuarioConEmailIncorrecto() {
        Usuario user = new Usuario();
        user.setEmail("noexiste@mail.com");
        user.setPassword("1234");
        LoginDto dto = new LoginDto(new Logger());
        assertFalse(dto.verificarUsuarioPorEmail(user));
    }

    // TODO add test methods here.
    // The methods must be annotated with annotation @Test. For example:
    //
    // @Test
    // public void hello() {}
}
