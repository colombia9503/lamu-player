/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import lamuclient.Common.Logger;
import lamuclient.DTO.UsuariosDto;
import lamuclient.POJOS.Usuario;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author santi
 */
public class UsuariosDtoTest {
    
    public UsuariosDtoTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    // TODO add test methods here.
    // The methods must be annotated with annotation @Test. For example:
    //
    // @Test
    // public void hello() {}
    
    @Test
    public void CuandoClaveIngresadaIgualAClaveVerificada() {
        UsuariosDto dto = new UsuariosDto(new Logger());
        assertTrue(dto.verificarClave("clave1", "clave1"));
    }
    
    @Test
    public void RegistroDeUsuarioConLaInformacionCorrecta() {
        UsuariosDto dto = new UsuariosDto(new Logger());
        Usuario user = new Usuario();
        user.setApellidos("de tales");
        user.setEmail("juanitodtales123");
        user.setNombres("juanito");
        user.setPassword("1234");
        user.setRol(1);
        assertTrue(dto.registrarNuevoUsuario(user));
    }
    
    @Test
    public void ModificarUsuarioConInformacionCorrecta() {
        UsuariosDto dto = new UsuariosDto(new Logger());
        Usuario user = new Usuario();
        user.setApellidos("modificado");
        user.setEmail("modificado123");
        user.setNombres("modific");
        user.setPassword("5555");
        user.setId(8);
        user.setRol(1);
        assertTrue(dto.actualizarUsuario(user));
    }
    
    @Test
    public void ModificarUsuarioConInformacionIncompleta() {
        UsuariosDto dto = new UsuariosDto(new Logger());
        Usuario user = new Usuario();
        user.setApellidos("modificado");
        user.setEmail("");
        user.setNombres("");
        user.setPassword("");
        user.setId(8);
        assertFalse(dto.actualizarUsuario(user));
    }
    
    @Test
    public void ModificarUsuarioQueNoExiste() {
        UsuariosDto dto = new UsuariosDto(new Logger());
        Usuario user = new Usuario();
        user.setApellidos("modificado");
        user.setEmail("modificado123");
        user.setNombres("modific");
        user.setPassword("5555");
        user.setId(55);
        user.setRol(1);
        assertFalse(dto.actualizarUsuario(user));
    }
    
    @Test
    public void EliminarUnUsuarioExistente() {
        UsuariosDto dto = new UsuariosDto(new Logger());
        Usuario user = new Usuario();
        user.setId(12);
        assertTrue(dto.borrarUsuario(user));
    }
    
    @Test
    public void EliminarUnUsuarioQueNoExiste() {
        UsuariosDto dto = new UsuariosDto(new Logger());
        Usuario user = new Usuario();
        user.setId(55);
        assertFalse(dto.borrarUsuario(user));
    }
            
}
