/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import java.io.File;
import java.io.IOException;
import lamuclient.Common.Logger;
import lamuclient.DTO.PistasDto;
import lamuclient.POJOS.Pista;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author santi
 */
public class PistasDtoTest {
    
    public PistasDtoTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    // TODO add test methods here.
    // The methods must be annotated with annotation @Test. For example:
    //
    // @Test
    // public void hello() {}
    @Test
    public void guardarPistaNuevaConInformacionCorrectaArchivoCorrectoMp3() throws IOException {
        PistasDto dto = new PistasDto(new Logger());
        Pista pista = new Pista();
        File archivo = new File(System.getProperty("user.dir") + "/src/lamuclient/Resources/audio/vetealaverga-2-verguisas-pop" );
        pista.setAlbumn("albumn");
        pista.setAutor(2);
        pista.setDescripcion("descripcion");
        pista.setDuracion("duracion");
        pista.setGenero("genero");
        pista.setNombre("nombre");
        assertTrue(dto.salvarNuevaPista(pista, archivo));
    }
    
    @Test 
    public void guardarPistaNuevaConInformacionIncompletaArchivoCorrecto() {
        PistasDto dto = new PistasDto(new Logger());
        Pista pista = new Pista();
        File archivo = new File(System.getProperty("user.dir") + "/src/lamuclient/Resources/audio/vetealaverga-2-verguisas-pop" );
        pista.setAlbumn("");
        pista.setAutor(1);
        pista.setDescripcion("");
        pista.setDuracion("");
        pista.setGenero("");
        pista.setNombre("");
        assertFalse(dto.salvarNuevaPista(pista, archivo));
    }
    
    @Test
    public void guardarPistaNuevaConArchivoIncorrecto() {
        PistasDto dto = new PistasDto(new Logger());
        Pista pista = new Pista();
        File archivo = new File(System.getProperty("user.dir") + "/src/lamuclient/Resources/audio/vetealaverga-2-verguisas-pop" );
        pista.setAlbumn("");
        pista.setAutor(1);
        pista.setDescripcion("");
        assertFalse(dto.salvarNuevaPista(pista, archivo));
    }
}
